Downloading
=================

Installers are available for Windows (XP, Vista, 7, 8) and for Max OS X (from 10.5 to 10.8).

To download the latest pre-compiled version of pyo, go to the pyo's googlecode `download <http://code.google.com/p/pyo/downloads/list>`_ page.

Under Debian distros, you can get pyo from the package manager. The library's name is **python-pyo**.
