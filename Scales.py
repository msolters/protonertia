#this construct is able to return arbitrary notes and intervals from different scales
# author:  mark j solters

class Key(object):

    notes = ['Cn', 'C#', 'Dn', 'D#', 'E', 'Fn', 'F#', 'Gn', 'G#', 'An', 'A#', 'B']
    major = [0, 2, 4, 5, 7, 9, 11]
    minor_harmonic = [0, 2, 3, 5, 7, 8, 11]
    minor_melodic = [0, 2, 3, 5, 7, 9, 11]

    def __init__(self, root, nature):
        self.currNote = root #the current note of interest (starts at the root)
        self.root = self.notes.index(root) #the position in the notes vector of the root
        self.keyNotes = [] #this vector contains the explicit notes for a given scale
        if nature == 'major':
            self.scale = self.major
        elif nature == 'minor_harmonic':
            self.scale = self.minor_harmonic
        elif nature == 'minor_melodic':
            self.scale = self.minor_melodic

        for tone in self.scale:
            nextNote = self.root + tone
            while nextNote > 11:
                nextNote = nextNote - 12

            self.keyNotes.append( self.notes[nextNote] )
        #print self.keyNotes


    def Step(self, startNote, n):
        currPos = self.keyNotes.index(startNote)
        #currPos = self.keyNotes.index(self.currNote)

        newPos = currPos + n
        if newPos > (len(self.keyNotes)-1):
            newPos = newPos - (len(self.keyNotes)-1) - 1
        elif newPos < 0:
            newPos = newPos + (len(self.keyNotes)-1)

        self.currNote = self.keyNotes[ newPos ]
        return self.currNote
