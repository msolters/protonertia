from pyo import *

#these sound event constructs are what we use for sample playback from the groove folders; THEY LOOP

class FXMulti(object):

    def __init__(self, src):
        defVolume=0.7 #global default, this prevents speaker clipping on most laptops
        self.sound = SfPlayer(src, speed=[1,1], loop=True, mul=defVolume).mix(2)
        self.reverbParameters = [1, 1, 0.5]
        self.delayParameters = [0.0, 0.0, 1]

        #delay before reverb otherwise you'll be timeshifting the echoes!
        #self.output = Delay(self.sound, delay=self.delayParameters[0], feedback=self.delayParameters[1], mul=self.delayParameters[2])
        self.output = Freeverb(self.sound, size=self.reverbParameters[0], damp=self.reverbParameters[1], bal=self.reverbParameters[2])

    def playChain(self):
        return self.output.out()

    def stopChain(self):
        self.output.stop()
