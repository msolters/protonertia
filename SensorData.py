import serial, time

class SensorData(object):
    attribute_names = [
            "a_x",
            "a_y",
            "a_z",
            "g_x",
            "g_y",
            "g_z",
            "button"
    ]

    def __init__(self, data_string):
        data_string = data_string.lstrip("[").rstrip("]\r\n")
        values = [float(value) for value in data_string.split(",")]
    #print values
        if len(values) != len(self.attribute_names):
    # We can't reasonably continue with a wrong number of values
            raise ValueError("Expected {} data values; got {}"
                    "".format(len(self.attribute_names), len(values)))
    # Assign the values to attributes
        for name, value in zip(self.attribute_names, values):
            setattr(self, name, value)

    @classmethod
    def yield_from_serial(cls, serial_object):
        #try:
        #for line in serial_object:
        try:
            data = cls(serial_object)
        except ValueError:
            pass
        except serial.serialutil.SerialException:
            pass
        else:
            yield data
        #except:# serial.serialutil.SerialException:
        #       print "Warning: bluetooth communication error
