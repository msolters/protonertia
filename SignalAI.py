#  signal processing articial intelligence for Chime
#  author = baron (mark j. solters)

import math, time, random

def choose_different(items, _last=[None]):

	while True:
		item = random.choice(items)
		if item is not _last[0]:
			_last[0] = item
			return item

class returnAvg(object):  #this algorithm returns an equally weighted average of the last N=histSize data points

	def __init__(self, histSize): #this code invokes a new instance of the algorithm with N=histSize
		self.N = int(histSize) #this is the number of data points to average
		self.reset() # initial values

	def addData(self, c):  #invoke this method to add a new data point, and return the recomputed average
		self.cVec.append(float(c)) #add data point to end of history vector
		self.length = len(self.cVec) #calculate new length of vector

		if self.length > self.N: #only keep last self.N data points
			self.cVec.pop(0) #delete the first (oldest) element

		if self.length > 1: #prevent division by 0 in the case where histSize \def 1 (cVec is a scalar)
			T = sum(element for element in self.cVec)
			self.currAvg = T/(self.length-1) #compute average
		else:
			self.currAvg = T = self.cVec[0]

		return self.currAvg

	def reset(self):
		self.cVec = []
		self.currAvg = None
		self.length = 0

class hist_vec(object): # keeps a vector with exactly N elements
	def __init__(self, histSize):
		self.N = int(histSize)
		self.cVec = []
		self.length = 0

	def addData(self, c):
		if self.length >= self.N:
			self.cVec.pop(0)
		else:
			self.length += 1
		self.cVec.append(c)




class signalIntegrator(object): #this algorithm integrates finite arrays of input in real time

	def __init__(self, noiseThreshold):  #data values with a magnitude less than noisethreshold are thrown out are not used in the integration
		self.timeMeta = time.time()
		self.currValue = 0
		self.noise = noiseThreshold

	def addData(self, c): #take the last value, integrate over time delay
		now = time.time()
		deltaT = now - self.timeMeta
		if abs(c) > self.noise:
			deltaC = deltaT * c
		else:
			deltaC = 0
		self.currValue += deltaC
		self.timeMeta = now
		return self.currValue

class modularWrapper(object):

	def __init__(self, a): #if input, c, is greater than limit a, the number starts over at 0
		self.a = a

	def fix(self, c):
		if c > self.a:
			return ( c - math.floor( c / self.a ) )
		else:
			return c

class lastDiff(object):
	def __init__(self):
		self.meta = None

	def compare(self, c):
		if self.meta != None:
			diff = c - self.meta
			self.meta = c
			return diff
		else:
			self.meta = c
			return 0


class setSwitch(object):

	def __init__(self, b = 0):
		self.mp = b

	def result(self, c):
		if c < self.mp:
			return -1
		elif c > self.mp:
			return 1
		else:
			return 0

class mapToIntegerRange(object):

	def __init__(self, cMin, cMax, oMin, oMax):
		self.cMin = cMin
		self.cMax = cMax
		self.oMin = oMin
		self.oMax = oMax

	def result(self, c):
		o = self.oMin + (c - self.cMin)*(self.oMax - self.oMin)/(self.cMax - self.cMin)
		return int(o)


class BumpFollower(object):  #this algorithm returns a normalized output (0, 1) that increases when the input paramter is over a specified minimum, and decays exponentially when the value falls below this
	def __init__(self, r, cr, cMin, cMax, name): #this code attempts to populate the attributes
		self.name = name

		self.trigger=False
		self.mnmThreshold = 0.0001
		self.r = r #output signal decay half-life (s) 
		self.cr = cr #trigger decay half-life (s)
		self.cMin = cMin #minimum expected input parameter value (output signal = 0)
		self.cMax = cMax #maximum expected input parameter value (output signal = 1)	
		

		self.cMeta = 0 #initialize an empty holder variable for last paramter value
		self.cLocalMax = 0 #output signal decay amplitude
		self.m = (self.cMax - self.cMin)
		self.outputIncreasing = False
		self.lastDecayTime = time.time()
		self.dT = 0.0
		self.output = self.outputMeta = 0

	def addData(self, c):
		t = time.time()
		self.dT = t - self.lastDecayTime
		if c > self.cMax: #over the max input, return 1.0
			self.output = 1.0
			c = self.cMax
		if self.cMax >= c and c > self.cMin:
			if c >= self.cMeta:  #parameter has increased since last reading
				self.outputIncreasing = True
				self.output = self.outputMeta = (c - self.cMin)/self.m # C -> O : [cMin, cMax] -> [0, 1.0]
				self.cMeta = c #save last parameter value
				self.lastDecayTime = time.time()
			
			else: #parameter has decreased since last reading
				self.outputDecay()

		else: #parameter is below mnm value
			self.outputDecay()

		
		if self.trigger == True:
			self.trigger = False
			return 'trigger event:'+str(self.output)
		else:
			return self.output
			
	def returnCurrentValue(self):
		self.dT = time.time() - self.lastDecayTime
		self.outputDecay()
		return self.output

	def outputDecay(self):
		if self.output < self.mnmThreshold:
			self.output = 0
		else:
			self.output = self.outputMeta * math.exp(-self.dT/self.r)
		
		if self.outputIncreasing == True:
			self.cLocalMax = self.cMeta
			self.outputIncreasing = False
			self.trigger = True

		if self.cMeta < self.mnmThreshold:
			self.cMeta = 0
		else:
			self.cMeta = self.cLocalMax*math.exp(-self.dT/self.cr)
