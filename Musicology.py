#this construct is able to return arbitrary notes and intervals from different scales
# author:  mark j solters

class Musicology(object):

    notes = ['Cn', 'C#', 'Dn', 'D#', 'E', 'Fn', 'F#', 'Gn', 'G#', 'An', 'A#', 'B']

    major = [0, 2, 4, 5, 7, 9, 11]
    minor_harmonic = [0, 2, 3, 5, 7, 8, 11]
    minor_melodic = [0, 2, 3, 5, 7, 9, 11]

    def getScaleMembers(self, key):
        # key will be a vector
        # the first element, [0] is the root note
        # the second element, [1] is the scale type
        key_root = key[0]
        key_type = key[1]

        root_i = self.notes.index(key_root)
        if key_type == "major":
            scale_i = self.major
        if key_type == "minor_harmonic":
            scale_i = self.minor_harmonic
        if key_type == "minor_melodic":
            scale_i = self.minor_melodic

        scale = []
        for i in scale_i:
            new_i = root_i + i
            while new_i > 11:
                new_i - 12
            scale.append(self.notes[new_i])
        return scale


    def getChordMembers(self, numeralChord, key):

        chord = []
        scale = self.getScaleMembers(key)

        #if len(numeralChord) > 2: # this will only come into play when we have non-triad chords
        numeral = numeralChord[0]

        lower_numeral = numeral.lower()
        if lower_numeral == 'i':
            root_pos = 0
        elif lower_numeral == 'ii':
            root_pos = 1
        elif lower_numeral == 'iii':
            root_pos = 2
        elif lower_numeral == 'iv':
            root_pos = 3
        elif lower_numeral == 'v':
            root_pos = 4
        elif lower_numeral == 'vi':
            root_pos = 5
        elif lower_numeral == 'vii':
            root_pos = 6

        root = self.notes.index(scale[root_pos])
        chord.append(self.notes[root])

        quality = False
        if numeral.islower():
            quality = "minor"
        else:
            quality = "major"

        if "minor" in quality:
            third_i = root + 3
            while third_i > 11:
                third_i = third_i - 12
            chord.append(self.notes[third_i])

        if "major" in quality:
            third_i = root + 4
            while third_i > 11:
                third_i = third_i - 12
            chord.append(self.notes[third_i])

        fifth_i = root + 7
        while fifth_i > 11:
            fifth_i = fifth_i - 12
        chord.append(self.notes[fifth_i])

        return chord
