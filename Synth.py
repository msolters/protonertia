from pyo import *
from ChannelClass import *
import linecache
import glob
import math

#this is essentially a live sound bank, like a midi patch
# author:  mark j solters (2013)

class Synth(object):

    def __init__(self, folderpath='~'):
        self.instrumentName = folderpath
        self.files=glob.glob(folderpath+"/*.wav")
        #print "For your amusement and edification we present the files inside", folderpath, ":", self.files
        self.Sounds = []
        self.event = 0

    def __del__(self):
        del self.files

    def clear(self):
        self.event = 0
        for sound in self.Sounds:
            sound.stop()
            self.Sounds = []

    def play(self, note, vol):
        noteIndex = self.getTrackIndex(note)
        if noteIndex != 404:  #we can find that note in memory vector
            N = 3
            if self.event > N:
                self.Sounds[self.event-N].stop()
                self.Sounds.pop(0)
                self.event = self.event - 1
            #cSound = ChannelClass(self.files[noteIndex], vol )
            try:
                self.Sounds.append(ChannelClass(self.files[noteIndex], vol))
                #self.cnote = ChannelClass(self.files[noteIndex], vol)
            except ValueError:
                print 'oops :('
                return

            #if cnote:
            #       cnote.playChain()
            #self.Sounds.append( ChannelClass( self.files[noteIndex], vol ) )  #we add another sound to the timeline
            self.Sounds[self.event].playChain()  #we play that sound
            self.event = self.event + 1      #index the event....
        else:
            print 'Sample for note ',note,'is mysteriously missing. Return code', noteIndex

    def getTrackIndex(self, track):
        #print 'looking for the note',track,'in:'
        for f in self.files:
            if track in f:
                return self.files.index(f)
        return 404

    def setGrooveVolume(self, Mul):
        self.grooveVolume = Mul
        for Sound in self.SfPInstn:
            self.changeVolume(self.SfPInstn.index(Sound), Sound)

    def setTrackVolume(self, track, Mul):
        trackIndex = self.getTrackIndex(track) #index of files
        print "track=",track,", and index=",trackIndex
        if trackIndex != None:
            self.trackVolumes[trackIndex]=Mul
            self.changeVolume(trackIndex, self.SfPInstn[trackIndex])

    def setTrackReverb(self, track, Rev):
        trackIndex = self.getTrackIndex(track) #index of files
        if trackIndex != None:
            self.SfPInstn[trackIndex].reverbParameters[2]=Rev
            self.SfPInstn[trackIndex].reverbParameters[1]=1-Rev

    def setTrackDelay(self, track, Del):
        trackIndex = self.getTrackIndex(track) #index of files
        if trackIndex != None:
            self.SfPInstn[trackIndex].delayParameters[0]=1-Del
            self.SfPInstn[trackIndex].reverbParameters[1]=Del

    def changeVolume(self, trackIndex, trackRef):
        trackRef.sound.setMul(self.grooveVolume*self.trackVolumes[trackIndex])

    def getNext(self, totalNum, currentNum):
        if (currentNum==totalNum-1):
            return 0
        else:
            return currentNum+1

    def getPrevious(self, totalNum, currentNum):
        if (currentNum==0):
            return totalNum-1
        else:
            return currentNum-1



    @property
    def SfPlayerInstances(self): return self.SfPInstn
