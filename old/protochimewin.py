'''
Created on April 5th 2013

@author: mark j. solters (mjs), juan david rios (jdr)
'''
from Leap import CircleGesture, KeyTapGesture, ScreenTapGesture, SwipeGesture
from Scales import *
from Synth import * #this loads sound banks of individual instruments
from Chart import Chart #this handles chord progressions per beat
from Groove import * #these handles sets of samples that go together
#from pyo import *
from pyolib import * #this is an audio engine
from SensorData import * #this code handles acquiring data over BT
from SignalAI import * #this code is where all signal analysis algorithms go

import Leap
import os, sys
import time
import math
import glob
import serial
import socket
import select
import subprocess
import argparse



def choose_different(items, _last=[None]):
    while True:
        item = random.choice(items)
        if item is not _last[0]:
            _last[0] = item
            return item

loopsToRepeat=2
crossFadeSize=8 #this is in units of beats
masterVolume=0.8 #this represents the final multiplier of our entire synthesizer



#first we make sure users have entered the necessary data as arguments
parser= argparse.ArgumentParser(description="musikbox")
parser.add_argument("-s", action="store", default="Synths/", dest="SynthPath", help="Folder containing synth samples.")
parser.add_argument("-g", action="store", default="Grooves/", dest="GroovePath", help="Folder containing groove charts and samples.")
args = parser.parse_args()


#####################################################################
#   Load resources!  (BLAM!)
#####################################################################
#First load paths to music data
SynthPath = args.SynthPath
GroovePath = args.GroovePath

#####################################################################
#   Connect to device!         (pow)
#####################################################################





#####################################################################
#   Load musical data structures from resource folders (!)
#####################################################################
#first we create a list of grooves!
synthList = []
SynthFolders=glob.glob(SynthPath+"/*")
#print SynthFolders
for folder in SynthFolders:
	synthList.append(Synth(folder))
	
#next a list of samples
groovePool = []
folders=glob.glob(GroovePath+"/*")
#print folders
for folder in folders:
	groovePool.append(Groove(folder))
groovePoolSize = len(groovePool)

grooveList = []  #finally we create and fill a list of charts
for groove in groovePool:
	grooveList.append(groove.chart)


#####################################################################
#   Start your enginessssssss......
#####################################################################
s = Server(sr=44100,buffersize=8192).boot() #boot the audio server
s.start() #start the audio server

for groove in groovePool:
	groove.loadParts()
	groove.setGrooveVolume(0)


_currentGroove = 0 #this variable keeps track of which groove we are playing; starts at the first one
#_currentSynth = 0

gyro_synth = 8#random.randint(0, (len(synthList)-1))
accel_synth = 4#random.randint(0, (len(synthList)-1))

doneYet=False #this variable is used to control the crossfader 
changeTriggered = False
changeTriggeredMeta = False

#establish timekeeping vars
lastGrooveChangeTime = time.time()
lastGrooveChangeTimeMeta = lastGrooveChangeTime

groovePool[_currentGroove].play()
groovePool[_currentGroove].setGrooveVolume(masterVolume)
changePoint = int(groovePool[_currentGroove].chart.time)*loopsToRepeat*int(float(groovePool[_currentGroove].chart.sampleLoopSize)) #number of beats until the next groove
transitioning=False #this variable is true when we are crossfading between adjacent grooves

approximateMeasuresMeta = 0


#declare SignalAI constructs here

a_Mag_BF = BumpFollower(3, 3, 2, 3, 'aMag')
azimuth_z = setSwitch()

z_threshold = 0.205 # threshold of azimuthal acceleration direction change in radians to trigger chord
z_angle_diff = lastDiff()

g_threshold = 60
roll_change = lastDiff()

noise = returnAvg(30)
noise_fluctuation = lastDiff()


g_Mag_BF = BumpFollower(3, 3, 130, 400, 'gMag')
g_Sum_avg = returnAvg(5)
g_Mag_avg = returnAvg(30)

nextStep = mapToIntegerRange(-750, 750, -1, 1)

#g_z_midPoint = returnAvg(2)
#g_z_integrator = signalIntegrator(1)

#angleModular = modularWrapper(360)






##############################################################################################
###################  chime lifecycle  ##################################################
##############################################################################################
volume = 0
launch = metaTime = time.time()
print 'Current instruments: ', synthList[gyro_synth].instrumentName, 'and', synthList[accel_synth].instrumentName
while True:
	#response = SensorData.yield_from_serial(ard)
	try:
		data = ard.readline()
		#print data
		if len(data) > 0:
			#print data
			response = SensorData.yield_from_serial(data)
			pass
		else:
			'zero data'
			continue
	except:# serial.serialutil.SerialException:
		'serial exception'
		continue
	
	for data in response:
		currentTime = time.time()
		cTime = currentTime-launch 
		mix = groovePool[_currentGroove]

		if data.button == 1: #change instruments by button press
			#synthList[gyro_synth].clear()
			#synthList[accel_synth].clear()
			if gyro_synth == (len(synthList)-1):
				gyro_synth = 0
			else:
				gyro_synth += 1
			#print 'Switched to instrument: ' + synthList[_currentSynth].instrumentName
			
			#gyro_synth = random.randint(0, (len(synthList)-1))#choose_different(range(0, (len(synthList)-1) ))
			accel_synth = random.randint(0, (len(synthList)-1))#choose_different(range(0, (len(synthList)-1) ))
			print 'Current instruments: ', synthList[gyro_synth].instrumentName, 'and', synthList[accel_synth].instrumentName			
			
		######################################################################################################
		#examples of using SignalAI classes to get normalized output parameters from input parameters (aka sensor values)


		#BumpFollower for magnitude of instantaneous gyro velocity
		gMagnitude = math.sqrt(data.g_x**2 + data.g_y**2 + data.g_z**2)
		g_mag_value = g_Mag_BF.addData(g_Mag_avg.addData(gMagnitude))
		g_delta = abs(roll_change.compare(gMagnitude))
		
		deltaNoise = noise_fluctuation.compare(data.g_z)
		g_z_t = noise.addData(deltaNoise)
		#print g_z_t
		

		#BumpFollower for magnitude of instananeous acceleration
		aMagnitude = math.sqrt(data.a_x**2 + data.a_y**2 + data.a_z**2)	
		a_mag_value = a_Mag_BF.addData(aMagnitude)
		




		######################################################################################################
		#now some examples of how to use those output parameters to play notes or change the volume, etc.

		if isinstance(g_mag_value, basestring) and 'trigger event' in g_mag_value:
			g_output = float(g_mag_value.split(":")[1])
		else:
			g_output = g_mag_value
		#print g_output


		z_angle = 0
		if (data.a_x != 0) or (data.a_y != 0):
			z_angle = math.atan( data.a_z / math.sqrt( data.a_x**2 + data.a_y**2 ) )
		
		timeD = currentTime - metaTime
		z_diff = abs(z_angle_diff.compare(z_angle)) #* (180/math.pi) / timeD this is for instantaneous velocity in degrees per second
		#z_speed = (z_diff/timeD)
		metaTime = currentTime

		if isinstance(a_mag_value, basestring) and 'trigger event' in a_mag_value:
			a_output = float(a_mag_value.split(":")[1])
			#changeTriggered = True
			#changeTriggeredMeta = True
		else:
			a_output = a_mag_value
		
		tonalProbability = a_output # (0, 1.0), less than this, output stays in the chord progression
		#print tonalProbability
		if g_delta > g_threshold:
			intensity = (1 - (g_threshold/g_delta))
			outcome = random.uniform(0, 1.0)
			note = 'foo'
			if outcome > tonalProbability: 
				note = choose_different(grooveList[_currentGroove].currentChord(cTime)) #always start on a different note, but in the chord progression
			elif tonalProbability > outcome:
				note = grooveList[_currentGroove].scale.Step( grooveList[_currentGroove].scale.currNote , random.randint(-3, 2)) #improvise in the current scale
			
			vol = random.uniform(0, 1.0)
			synthList[ gyro_synth ].play(note, vol*intensity*0.2)
			#time.sleep(0.09*intensity)		


		if z_diff > z_threshold and random.uniform(0, 1.0) < 0.4:
			intensity = (1 - (z_threshold/z_diff))
			outcome = random.uniform(0, 1.0)
			note = 'foo'
			if outcome > tonalProbability: 
				note = choose_different(grooveList[_currentGroove].currentChord(cTime)) #always start on a different note, but in the chord progression
			elif tonalProbability > outcome:
				note = grooveList[_currentGroove].scale.Step( grooveList[_currentGroove].scale.currNote , random.randint(-5, 7)) #improvise in the current scale
			
			vol = random.uniform(0, 1.0)
			synthList[ accel_synth ].play(note, vol*intensity*0.05)
			time.sleep(0.09*intensity)

		
		
		
		timeElapsed = currentTime-lastGrooveChangeTime
		beatsElapsed = float(groovePool[_currentGroove].chart.tempo) * timeElapsed / 60


		######################################################################################################
		# use acceleration bump follower to trigger scene shift (groove samples) - (NOT WORKING YET)


			
		if changeTriggered and changeTriggeredMeta:  #important!!!!
			changePoint = beatsElapsed + crossFadeSize/2 #sets changepoint to one half crossfade distance in time from now
			changeTriggeredMeta = False
			print 'change'
		else:
			changePoint = int(groovePool[_currentGroove].chart.time)*loopsToRepeat*int(float(groovePool[_currentGroove].chart.sampleLoopSize)) #number of beats until the next groove
			
			
		###############################################################################
		#       Ambient Tracks 
		###############################################################################
		#  Spin Track
		mix.setTrackVolume("Drums.wav", a_output)
		mix.setTrackVolume("jazzPercussion_vaigorr.wav", a_output)
		mix.setTrackVolume("jazzPercussion_ishtin.wav", a_output)
		mix.setTrackVolume("DeepSpacesUnderGrove.wav", 0.5*a_output)
		
		# Impact Tracks
		mix.setTrackVolume("DeepSpaces.wav", g_output*0.7)  #these set background sample volumes by file name		
		mix.setTrackVolume("harmonicFuzz.wav", a_output*0.3)	
		mix.setTrackVolume("guitar.wav", a_output*0.4)
		mix.setTrackVolume("rnbPercussion.wav", a_output*0.7)
			
			
		currentOffset = changePoint - beatsElapsed  #time till crossfade
	
		if len(grooveList) > 1: #only crossfade if we have more than one track (can't fadein and fadeout the same track)
			if ((-crossFadeSize/2 < currentOffset) and (currentOffset < crossFadeSize/2)): #we are on the left side of the crossfade
				onTheWayDown=0.5+currentOffset/crossFadeSize
				onTheWayUp=0.5-currentOffset/crossFadeSize
				groovePool[_currentGroove].setGrooveVolume(onTheWayDown*masterVolume)
				lastGrooveChangeTimeMeta = currentTime
				groovePool[groovePool[_currentGroove].getNext(groovePoolSize, _currentGroove)].setGrooveVolume(onTheWayUp*0.5*masterVolume)
			
	
			elif (currentOffset < -crossFadeSize/2): #we are past the crossfade region.  perhaps turn off loops now?
				lastGrooveChangeTime = lastGrooveChangeTimeMeta
				changePoint = changePointMeta #resets changepoint for next groove according to chart
				_currentGroove = _currentGrooveMeta
#				groovePool[groovePool[_currentGroove].getPrevious(groovePoolSize, _currentGroove)].setGrooveVolume(0)
				groovePool[groovePool[_currentGroove].getPrevious(groovePoolSize, _currentGroove)].stop()
				changeTriggered = False
				transitioning=False
	
		#sequencing tracks based on the set number of loops
			if doneYet == True: #this case happens when a track is ready to change
				#groovePool[]
				groovePool[_currentGrooveMeta].play()
				changePointMeta = int(groovePool[_currentGroove].chart.time)*loopsToRepeat*int(float(groovePool[_currentGrooveMeta].chart.sampleLoopSize))
				doneYet = False
				transitioning = True
			else:
				measuresElapsed = beatsElapsed/float(groovePool[_currentGroove].chart.time)
				approximateMeasures = round(measuresElapsed) #for display and debug purposes only
				
				if approximateMeasures != approximateMeasuresMeta:
					approximateMeasuresMeta = approximateMeasures		
				#print "Measure: {}".format(approximateMeasuresMeta) #ditto
	
				if ((True != transitioning) and (measuresElapsed > changePoint/4-0.00001)):
					_currentGrooveMeta=groovePool[_currentGroove].getNext(groovePoolSize, _currentGroove)
					doneYet=True
	#ard.close()
	#ard.flush()

	#time.sleep(0.001)

s.gui(locals())
