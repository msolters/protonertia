from pyo import *
from FXMulti import *
from Chart import *
import linecache
import glob
import math
# this class contains information about samples and arrays of samples
#by mark j.solters
class Groove(object):

    def __init__(self, folderpath='~'):
        self.files=glob.glob(folderpath+"/*.wav")
        #print 'Loading Groove Files from',folderpath,':',self.files
        self.chart=Chart(folderpath+'/Chart')
        self.SfPInstn = []
        self.trackVolumes = []
        self.grooveVolume = 1.0

    def __del__(self):
        del self.files

    def loadParts(self):
        defVolume = 0.7
        for fileName in self.files:
            self.SfPInstn.append(FXMulti(fileName))
            self.trackVolumes.append(defVolume)

    def play(self):
        for Sound in self.SfPInstn:
            Sound.playChain()

    def stop(self):
        for Sound in self.SfPInstn:
            Sound.stopChain()

    def getTrackIndex(self, track):
        for f in self.files:
            if track in f:
                return self.files.index(f)

    def setGrooveVolume(self, Mul):
        self.grooveVolume = Mul
        for Sound in self.SfPInstn:
            self.changeVolume(self.SfPInstn.index(Sound), Sound)

    def setTrackVolume(self, track, Mul):
        trackIndex = self.getTrackIndex(track) #index of files
        #print "track=",track,", and index=",trackIndex
        if trackIndex != None:
            self.trackVolumes[trackIndex]=Mul
            self.changeVolume(trackIndex, self.SfPInstn[trackIndex])

    def setTrackReverb(self, track, Rev):
        trackIndex = self.getTrackIndex(track) #index of files
        if trackIndex != None:
            self.SfPInstn[trackIndex].reverbParameters[2]=Rev
            self.SfPInstn[trackIndex].reverbParameters[1]=1-Rev

    def setTrackDelay(self, track, Del):
        trackIndex = self.getTrackIndex(track) #index of files
        if trackIndex != None:
            self.SfPInstn[trackIndex].delayParameters[0]=1-Del
            self.SfPInstn[trackIndex].reverbParameters[1]=Del

    def changeVolume(self, trackIndex, trackRef):
        trackRef.sound.setMul(self.grooveVolume*self.trackVolumes[trackIndex])

    def getNext(self, totalNum, currentNum):
        if (currentNum==totalNum-1):
            return 0
        else:
            return currentNum+1

    def getPrevious(self, totalNum, currentNum):
        if (currentNum==0):
            return totalNum-1
        else:
            return currentNum-1



    @property
    def SfPlayerInstances(self): return self.SfPInstn
