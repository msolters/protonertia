'''
Created on April 5th 2013

@author: mark j. solters (mjs)
'''

import Leap
from Leap import CircleGesture, KeyTapGesture, ScreenTapGesture, SwipeGesture
from Scales import *
from Musicology import *
from Synth import * #this loads sound banks of individual instruments
from Chart import Chart #this handles chord progressions per beat
from Groove import * #these handles sets of samples that go together
from pyo import *
from SignalAI import * #this code is where all signal analysis algorithms go

import os, sys
import time
import math
import glob
import serial
import socket
import select
import subprocess
import argparse

import numpy as np
from scipy import linalg
from sys import stderr
from math import sqrt

def fit_hypersphere(data, method="Hyper"):
        num_points = len(data)
#    print >>stderr, "DEBUG: num_points=", num_points

        if num_points==0:
            return (0,None)
        if num_points==1:
            return (0,data[0])
        #dimen = len(data[0])          # dimensionality of hypersphere
        dimen = 3
#    print >>stderr, "DEBUG: dimen=", dimen

        if num_points<dimen+1:
            raise ValueError("Error: fit_hypersphere needs at least {} points to fit {}-dimensional sphere, but only given {}".format(dimen+1,dimen,num_points))

# squared magnitude for each tuple, as a column vector
        square_mag = np.matrix( [sum(a*a for a in d) for d in data] ).transpose()

#    print >>stderr, "DEBUG: square_mag=", square_mag

# central n columns of matrix
        center = np.asmatrix(data, dtype=float)
#    print >>stderr, "DEBUG: center=", center

# matrix of data
        data_M = np.bmat( [[square_mag, center, np.ones((num_points,1))]])

#    print >> stderr, "DEBUG: data_M=",data_M

# matrix of 2nd moments:
        M = data_M.transpose() * data_M
        Means = np.mean(data_M, axis=0)

#    print >> stderr, "DEBUG: M=",M
#    print >> stderr, "DEBUG: Means=",Means

# construct constraint matrix
        N = np.asmatrix(np.identity(dimen+2, dtype=float))
        N[0,0] = 0
        N[-1,-1]=0
        if method=="Hyper":
            row = 4*Means
            row[0,-1]=2
            N[0,:] = row
            N[:,0] += row.transpose()
        elif method=="Pratt":
# constraint is sum of squares for coordinates minus
# 4 *
            N[0,-1]= -2
            N[-1,0]=-2
        elif method=="Taubin":
            row = 2*Means
            row[0,-1]=0
            N[0,:] = row
            N[:,0] += row.transpose()
        elif method=="Kasa":
            N[0,0]=1
            for i in range(1,dimen+1):
                N[i,i]=0
        else:
            raise ValueError("Error: unknown method: {} should be one of 'Hyper', 'Pratt', or 'Taubin'")
#    print >> stderr, "DEBUG:", method, "N=",N

        try:
            eigen_vals,eigen_vects = linalg.eigh(N,M)
# M is positive definite, so needs to go in the "b" position
#       but this means that our eigenvalues are inverted
#       so we want the largest eigenvalue, not the smallest positive one
        except np.linalg.linalg.LinAlgError:
# probably M singular (noise so low that we have perfect hypersphere)
#       How do I get an eigenvector corresponding to a zero eigenvalue?
            raise ValueError("OOPS: eigh error in {}, haven't figured out what the workaround is yet".format(method))

# I'm assuming that the eigenvalues are all real, as they should be for a Hermitian matrix

#    print >> stderr, "DEBUG: eigen_vals=", eigen_vals
#    print >> stderr, "DEBUG: eigen_vects=", eigen_vects
#    print >> stderr, "DEBUG: list(eigen_vals)=", list(eigen_vals)


        param_vect =eigen_vects[:,list(eigen_vals).index(max(eigen_vals))].transpose()
#    print >> stderr, "DEBUG: param_vect=", param_vect
#    params = np.asarray(param_vect)[0]         # numpy gets 1xn matrix for param_vect, scipy gets array of n
        params = np.asarray(param_vect)
#    print >> stderr, "DEBUG: params=", params
        radius = 0.5* sqrt( sum(a*a for a in params[1:-1])- 4*params[0]*params[-1])/abs(params[0])
        center = tuple(-0.5*params[1:-1]/params[0])
        return (radius,center)

class Session(object): # this class contains up-to-date information about the current musical environments

    def __init__(self):
        self.note_vec_meta = [0, 0, 0, 0, 0]
        self.key = 'Cn'
        self.key_type = 'major'
        self.bpm = 127
        self.chord = 'I'
        self.reset()

    def reset(self):
        self.meta_hands = []

    def checkTime(self, note_vec): #method for checking quantization using boolean logic
        note_vec_changes = [False, False, False, False, False]
        for i,num in enumerate(self.note_vec_meta):
            if note_vec[i] != num:
                note_vec_changes[i] = True
                self.note_vec_meta[i] = note_vec[i]
        return note_vec_changes

    currentSynth = 0 # tool for getting valid synths
    synthLen = 0

    def nextSynth(self):
        self.currentSynth += 1
        if self.currentSynth == self.synthLen:
            self.currentSynth = 0

class meta_hand(object):

    missed_frame_threshold = 3 # how many frames must be missed in order for the hand to be deleted from the data tracking vector

    def __init__(self, id):
        self.id = id
        self.missed_frames = 0
        self.last_known_position = None
        self.delta_hist = hist_vec(50)

    def new_position(self, new_pos):
        if self.last_known_position is not None:
            delta_vec = new_pos - self.last_known_position
            self.delta_hist.addData(delta_vec)
        self.last_known_position = new_pos

    def missFrame(self):
        self.missed_frames += 1
        if self.missed_frames >= self.missed_frame_threshold:
            return True
        else:
            return False

    def missFrame_reset(self):
        self.missed_frames = 0


class Listener(Leap.Listener):

    def on_init(self, controller):
        self.frontHandID = None
        print "The listener is ready."

    def on_connect(self, controller):
        print "Leap controller connected."

    def on_disconnect(self, controller):
        print "Disconnected"

    def on_exit(self, controller):
        print "exited"

    def on_frame(self, controller):  #gesture processing goes here
        frame = controller.frame()

        if not frame.hands.is_empty: #there ARE hands in this frame
            hand_ids = []
            for hand in frame.hands:
                hand_ids.append(hand.id)

            meta_hand_ids = []
            for m_hand in sesh.meta_hands:
                meta_hand_ids.append(m_hand.id)

            # next we do housekeeping on the list constructs
            meta_add = []
            for h_id in hand_ids:
                if h_id not in meta_hand_ids:
                    meta_add.append(h_id)
            for m_a in meta_add:
                sesh.meta_hands.append(meta_hand(m_a)) #add a new meta hand with this id

            meta_rem = [] # this logic only deletes hands that have been absent for the number of frames specified inside the meta_hand class
            for m_id in meta_hand_ids:
                if m_id not in hand_ids:
                    meta_rem.append(m_id)
            for m_r in meta_rem:
                m_target = None
                for m in sesh.meta_hands:
                    if m.id == m_r:
                        if m.missFrame() is True:
                            m_target = m
                        break
                if m_target is not None:
                    sesh.meta_hands.remove(m_target) #remove nonexistant hand from meta hand list

            for mhand in sesh.meta_hands: # finally, we analyze each hand in the data construct
                hand = frame.hand(mhand.id) # the actual leap motion hand construct
                stabilized_palm = hand.stabilized_palm_position # position of the palm
                mhand.new_position(stabilized_palm)

# display output for each hand
            out = ''
            for x in sesh.meta_hands:
                out += str(x.id)
                out += str('\t')
            out += '\n '
            for x in sesh.meta_hands:
                #if len(x.delta_hist.cVec) >= 4:
                    #test = fit_hypersphere(x.delta_hist.cVec, "Hyper")
                    #print test
                out += '\t'
#            for j in range(0, 49):
#                for x in sesh.meta_hands:
#                    if len(x.delta_hist.cVec) >= j+1:
#                        out += str(x.delta_hist.cVec[j])
#                    out += '\t'
#                out += '\n'
#            os.system('clear')
            print out


    def state_string(self, state):
        if state == Leap.Gesture.STATE_START:
            return "STATE_START"
        if state == Leap.Gesture.STATE_UPDATE:
            return "STATE_UPDATE"
        if state == Leap.Gesture.STATE_STOP:
            return "STATE_STOP"
        if state == Leap.Gesture.STATE_INVALID:
            return "STATE_INVALID"





#loopsToRepeat=float(raw_input('Groove repeats = '))
loopsToRepeat=2
crossFadeSize=8 #this is in units of beats
masterVolume=0.8 #this represents the final multiplier of our entire synthesizer



#first we make sure users have entered the necessary data as arguments
parser = argparse.ArgumentParser(description="Leap Motion Audio Synthesizer")

parser.add_argument("-s", action="store", default="Synths/", dest="SynthPath", help="Folder containing synth samples.")
parser.add_argument("-g", action="store", default="Grooves/", dest="GroovePath", help="Folder containing groove charts and samples.")
args = parser.parse_args()


#####################################################################
#   Load resources!  (BLAM!)
#####################################################################
#First load paths to music data
SynthPath = args.SynthPath
GroovePath = args.GroovePath

#####################################################################
#   Connect to device!         (pow)
#####################################################################
sesh = Session() # this variable contains all the music parameters on a frame-by-frame basis
listener = Listener()
controller = Leap.Controller()
controller.set_policy_flags(Leap.Controller.POLICY_BACKGROUND_FRAMES) #allows frame data to be gathered even when window is not in focus
controller.add_listener(listener)

#####################################################################
#   Load musical data structures from resource folders (!)
#####################################################################
#first we create a list of grooves!
synthList = []
SynthFolders=glob.glob(SynthPath+"/*")
#print SynthFolders
for folder in SynthFolders:
    synthList.append(Synth(folder))
sesh.synthLen = len(synthList)

#next a list of samples
groovePool = []
folders=glob.glob(GroovePath+"/*")
#print folders
for folder in folders:
    groovePool.append(Groove(folder))
groovePoolSize = len(groovePool)
#print "I found {} grooves in the local folder: {}".format(groovePoolSize, GroovePath)

grooveList = []  #finally we create and fill a list of charts
for groove in groovePool:
    grooveList.append(groove.chart)

#####################################################################
#   Start your enginessssssss......
#####################################################################
s = Server(sr=44100,buffersize=8192).boot() #boot the audio server
s.start() #start the audio server

#for groove in groovePool:
#       groove.loadParts()
#       groove.setGrooveVolume(0)


_currentGroove = 0 #this variable keeps track of which groove we are playing; starts at the first one

doneYet=False #this variable is used to control the crossfader
changeTriggered = False
changeTriggeredMeta = False

#establish timekeeping vars
lastGrooveChangeTime = time.time()
lastGrooveChangeTimeMeta = lastGrooveChangeTime
note_32nd = 1.0/32
note_16th = 1.0/16
note_8th = 0.125
note_4th = 0.25
note_2nd = 0.5

# background effects
groovePool[_currentGroove].play()
groovePool[_currentGroove].setGrooveVolume(masterVolume)
changePoint = int(groovePool[_currentGroove].chart.time)*loopsToRepeat*int(float(groovePool[_currentGroove].chart.sampleLoopSize)) #number of beats until the next groove
transitioning=False #this variable is true when we are crossfading between adjacent grooves

approximateMeasuresMeta = 0

musicology = Musicology() # this class contains methods pertaining to music theory

##############################################################################################
##############################################################################################
##############################################################################################
volume = 0
launch = metaTime = time.time()
while True:

    ##################################################################
    # this code handles quantization computations
    #######################################################################
    currentTime = time.time()
    cTime = currentTime-launch
    timeElapsed = currentTime-lastGrooveChangeTime
    beatsElapsed = float(sesh.bpm) * timeElapsed / 60
    beatsMeasure = beatsElapsed/groovePool[_currentGroove].chart.time
    beatsMeasure = beatsMeasure - int(beatsMeasure)
    note_vec = [int(beatsMeasure / note_2nd), int(beatsMeasure / note_4th), int(beatsMeasure / note_8th), int(beatsMeasure / note_16th), int(beatsMeasure / note_32nd)]
    [note2, note4, note8, note16, note32] = sesh.checkTime(note_vec) #tells us if we're on one of these notes

    #########################################################################################
    #       next we analyze the current kinetic data in our sesh
    ##########################################################################################


    #print hand_speed, dirZ
    hand_speed = random.uniform(0, 0.6)


    ######################################################################################
    # this construct allows certain events to only happen on a quantized note grid
    #####################################################################################

    #noteLength = 0
    #while noteLength == 0:
        #note = grooveList[_currentGroove].scale.Step( grooveList[_currentGroove].scale.currNote , dirZ)
##                      #if hand_speed > 0.5:
                #note = choose_different(grooveList[_currentGroove].currentChord(cTime))
            #noteLength = 32
#                       continue
#               if note16 is True:
#                       #if hand_speed > 0.4:
                #note = grooveList[_currentGroove].scale.Step( grooveList[_currentGroove].scale.currNote , dirZ)
            #noteLength = 16
#                       continue
#               if note8 is True:
            #if hand_speed > 0.28:
                #note = choose_different(grooveList[_currentGroove].currentChord(cTime))
            #noteLength = 8
#                       continue
#               if note4 is True:
            #if hand_speed > 0.19:
                #note = choose_different(grooveList[_currentGroove].currentChord(cTime))
            #       noteLength = 4
#                       continue
#               noteLength = 1

    note_test = None  # first we want to map the sesh's current key and numeral to a vector of octave-less note names
    if hand_speed > 0.1:
        note_test = musicology.getChordMembers([sesh.chord], [sesh.key, sesh.key_type])
    if hand_speed > 0.2:
        note_test = musicology.getChordMembers([sesh.chord], [sesh.key, sesh.key_type])
    if hand_speed > 0.3:
        note_test = musicology.getChordMembers([sesh.chord], [sesh.key, sesh.key_type])
    if hand_speed > 0.5:
        note_test = musicology.getChordMembers([sesh.chord], [sesh.key, sesh.key_type])

    if note_test is not None and note8 is True:
        note = choose_different(note_test)
        #print hand_speed, note, '/', note_test
        synthList[ sesh.currentSynth ].play(note, 0.8)
        #time.sleep(1.0/noteLength)

    #####################################################################################################
    # this code handles crossfading between grooves
    #####################################################################################################
    if changeTriggered and changeTriggeredMeta:  #important!!!!
        changePoint = beatsElapsed + crossFadeSize/2 #sets changepoint to one half crossfade distance in time from now
        changeTriggeredMeta = False
        print 'change'
    else:
        changePoint = int(groovePool[_currentGroove].chart.time)*loopsToRepeat*int(float(groovePool[_currentGroove].chart.sampleLoopSize)) #number of beats until the next groove

    currentOffset = changePoint - beatsElapsed  #time till crossfade

    if len(grooveList) > 1: #only crossfade if we have more than one track (can't fadein and fadeout the same track)
        if ((-crossFadeSize/2 < currentOffset) and (currentOffset < crossFadeSize/2)): #we are on the left side of the crossfade
            onTheWayDown=0.5+currentOffset/crossFadeSize
            onTheWayUp=0.5-currentOffset/crossFadeSize
            groovePool[_currentGroove].setGrooveVolume(onTheWayDown*masterVolume)
            lastGrooveChangeTimeMeta = currentTime
            groovePool[groovePool[_currentGroove].getNext(groovePoolSize, _currentGroove)].setGrooveVolume(onTheWayUp*0.5*masterVolume)


        elif (currentOffset < -crossFadeSize/2): #we are past the crossfade region.  perhaps turn off loops now?
            lastGrooveChangeTime = lastGrooveChangeTimeMeta
            changePoint = changePointMeta #resets changepoint for next groove according to chart
            _currentGroove = _currentGrooveMeta
#                       groovePool[groovePool[_currentGroove].getPrevious(groovePoolSize, _currentGroove)].setGrooveVolume(0)
            groovePool[groovePool[_currentGroove].getPrevious(groovePoolSize, _currentGroove)].stop()
            changeTriggered = False
            transitioning=False
            #sequencing tracks based on the set number of loops
        if doneYet == True: #this case happens when a track is ready to change
            #groovePool[]
            groovePool[_currentGrooveMeta].play()
            changePointMeta = int(groovePool[_currentGroove].chart.time)*loopsToRepeat*int(float(groovePool[_currentGrooveMeta].chart.sampleLoopSize))
            doneYet = False
            transitioning = True
        else:
            measuresElapsed = beatsElapsed/float(groovePool[_currentGroove].chart.time)
            approximateMeasures = round(measuresElapsed) #for display and debug purposes only

            if approximateMeasures != approximateMeasuresMeta:
                approximateMeasuresMeta = approximateMeasures
            #print "Measure: {}".format(approximateMeasuresMeta) #ditto
            if ((True != transitioning) and (measuresElapsed > changePoint/4-0.00001)):
                _currentGrooveMeta=groovePool[_currentGroove].getNext(groovePoolSize, _currentGroove)
                doneYet=True

    time.sleep(0.001)

s.gui(locals())
