from Scales import *
from pyo import *
from ChannelClass import *
import linecache
import glob
import math

#THIS CLASS CONTAINS MUSIC PARAMETERS SUCH AS TEMPO, CHORD PROGRESSIONS, CHANGES, ETC.
#THEY ARE READ FROM GROOVES\*\CHART AS TEXT FILES
#THEY CONTAIN INFORMATION ABOUT THE BASE KEY THEY ARE IN
# bY MARK J.SOLTERS

class Chart(object):

    def __init__(self, file='~'):
        self.chordLengths = []
        self.chords = []
        self.measureOffset = 0
        self.loopLength = 0

        with open(file) as f:
            content = f.readlines()
        for i,elem in enumerate(content):
            content[i]=elem.rstrip()


        self.tempo=int(content[0])
        self.time=int(content[1])
        self.sampleLoopSize=int(content[2])
        (keyRoot, keyNature) = content[3].split(' ')
        self.scale = Key(keyRoot, keyNature)


        for i in range(4, len(content)):
            cLine = content[i].split('\t')
            self.chordLengths.append(int(cLine[0])) #create a list of the lengths of each chord
            self.loopLength = self.loopLength + int(cLine[0])       #calculate the length of the musical passage, in bars
            cChordVector = []
            for l in xrange(1, (len(cLine))):
                cChordVector.append(cLine[l])
            self.chords.append(cChordVector)
        self.loopSize = len(self.chordLengths)
        #print self.chordLengths
        #print self.chords

    def currentChord(self, cTime):
        fixedTime=cTime
        fixedTime=fixedTime/60
        currentMeasure = round(self.tempo*(fixedTime)/self.time)-self.measureOffset+1
        #print currentMeasure
        tryChord = 0
        c = 0
        for length in self.chordLengths:
            tryChord = tryChord + length
            if tryChord >= currentMeasure:
                break
            else:
                c=c+1
        if c > (self.loopSize-1):
            self.measureOffset = self.measureOffset+self.loopLength
            return self.chords[0]
        else:
            return self.chords[c]
