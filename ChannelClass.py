from pyo import *

#this class is used to create individual synth notes as sound events using pyo library!

class ChannelClass(object):

    def __init__(self, src, vol):
        #print src
        defVolume=0.5 #global default, this prevents speaker clipping on most laptops
        try:
            self.sound = SfPlayer(src, speed=[1,1], loop=False, mul=vol).mix(2)
        except:
            raise ValueError("Could not find .wav for note {}".format(src))
            return
        #self.reverbParameters = [0.3, 0.5, 0.5]
        #self.delayParameters = [0.5, 0.1, 0.5]

        #delay before reverb otherwise you'll be timeshifting the echoes!
        #self.output = Delay(self.sound, delay=self.delayParameters[0], feedback=self.delayParameters[1], mul=self.delayParameters[2])
        #self.output = Freeverb(self.sound, size=self.reverbParameters[0], damp=self.reverbParameters[1], bal=self.reverbParameters[2])
        self.output = self.sound


    def playChain(self):
        self.output.out()

    def stop(self):
        self.output.stop()
